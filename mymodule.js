function testFunction() {
    console.log("hello from my module");
}
exports.testFunction = testFunction;
var MyClass = (function () {
    function MyClass() {
    }
    MyClass.prototype.myMethod = function () {
        console.log("hello from MyClass::myMethod");
    };
    return MyClass;
})();
exports.MyClass = MyClass;
