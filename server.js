/// <reference path="definitions/node/node.d.ts" />
/// <reference path="mymodule.ts" />
var http = require("http");
var mymodule = require('./mymodule');
var fs = require("fs");
mymodule.testFunction();
var mc = new mymodule.MyClass();
mc.myMethod();
http.createServer(function (req, res) {
    var fileContents = new Buffer("");
    if (req.url == "/") {
        fileContents = fs.readFileSync("test.html");
    }
    if (req.url == "/testClient.js") {
        fileContents = fs.readFileSync("." + req.url);
    }
    res.write(fileContents);
    res.end();
}).listen(8080);
