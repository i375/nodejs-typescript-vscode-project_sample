/// <reference path="definitions/node/node.d.ts" />
/// <reference path="mymodule.ts" />
/// <reference path="mypack/mymodule2.ts" />


import http = require("http")
import mymodule = require('./mymodule')
import myPackMod = require('./mypackage/mymodule2')
import fs = require("fs")

mymodule.testFunction()
var mc = new mymodule.MyClass()
mc.myMethod()


http.createServer((req,res)=>{
	
	var fileContents:Buffer = new Buffer("")
	
	if(req.url == "/")
	{
		fileContents = fs.readFileSync("test.html")
	}
	
	if(req.url == "/testClient.js")
	{
		fileContents = fs.readFileSync("."+req.url)
	}
	
	res.write(fileContents)
	res.end()
	
}).listen(8080)
